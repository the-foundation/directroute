# Directroute
.
script to check and add routes that bypass default routes ( e.g. vpn oder split-horizon networks )

used after if-up or in /etc/rc.local
---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/directroute/README.md/logo.jpg" width="480" height="270"/></div></a>
